package com.example.textwight;

import java.util.Date;

import android.os.Bundle;
import android.provider.AlarmClock;
import android.provider.Settings;
import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.RemoteViews;

public class MainActivity extends  AppWidgetProvider{

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Intent it = new Intent();
        String action = intent.getAction();
        ComponentName comp = new ComponentName("com.android.gallery3d","com.android.gallery3d.app.Gallery");//第一个参数是目标包名，第二个参数是目标包的activity名
        it.setComponent(comp);
        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        it.setAction(Intent.ACTION_MAIN);
        if (AppWidgetManager.ACTION_APPWIDGET_UPDATE.equals(action)) {
            RemoteViews views = new RemoteViews(context.getPackageName(),
                    R.layout.firstappwidget);
           
            views.setOnClickPendingIntent(R.id.tvMsg, PendingIntent.getActivity(context, 0, 
            		it, 0));
            int[] appWidgetIds = intent.getIntArrayExtra(
                    AppWidgetManager.EXTRA_APPWIDGET_IDS);

            AppWidgetManager gm = AppWidgetManager.getInstance(context);
            gm.updateAppWidget(appWidgetIds, views);
	}
	}
}
